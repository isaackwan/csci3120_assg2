package visitor;

import syntaxtree.*;

public class Task3Visitor extends Task2Visitor {

    @Override
    public void visit(StmtExprList n) {
        for (Statement statement : n.list) {
            System.out.print("    ");
            statement.accept(this);
            System.out.println(";");
        }
    }

    @Override
    public void visit(LocalVariableDeclaration n) {
        for (Assign2 a : n.list) {
            System.out.print("    ");
            n.firstType.accept(this);
            System.out.print(" ");
            a.accept(this);
        }
    }

    @Override
    public void visit(For n) {
        n.forInit.accept(this);
        System.out.print("while (");
        n.exp.accept(this);
        System.out.print(") {");
        n.statement.accept(this);
        System.out.println();
        n.stmtExprList.accept(this);
        System.out.println("}");
    }
}
