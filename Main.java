import syntaxtree.*;
import visitor.*;
import myparser.*;

public class Main {
   public static void main(String [] args) throws ParseException {
      Program root = new MiniJavaParser(System.in).Goal();
      root.accept(new Task3Visitor());
   }
}