package syntaxtree;
import visitor.Visitor;
import visitor.TypeVisitor;

public interface ForInit {
  public abstract void accept(Visitor v);
  public abstract Type accept(TypeVisitor v);
}
