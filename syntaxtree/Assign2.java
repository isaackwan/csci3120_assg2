package syntaxtree;

import visitor.TypeVisitor;
import visitor.Visitor;

public class Assign2 extends Statement {
  public Identifier i;
  public Exp e;

  public Assign2(Identifier ai, Exp ae) {
    i=ai; e=ae; 
  }

  public void accept(Visitor v) {
    v.visit(this);
  }

  public Type accept(TypeVisitor v) {
    return v.visit(this);
  }
}

