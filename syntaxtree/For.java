package syntaxtree;

import visitor.TypeVisitor;
import visitor.Visitor;

public class For extends Statement {
  public ForInit forInit;
  public Exp exp;
  public StmtExprList stmtExprList;
  public Statement statement;

  public For(ForInit forInit, Exp exp, StmtExprList stmtExprList, Statement statement) {
    this.forInit = forInit;
    this.exp = exp;
    this.stmtExprList = stmtExprList;
    this.statement = statement;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }

  public Type accept(TypeVisitor v) {
    return v.visit(this);
  }
}

