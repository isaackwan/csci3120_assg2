package syntaxtree;

import visitor.TypeVisitor;
import visitor.Visitor;

import java.util.Vector;

public class StmtExprList implements ForInit {
  public Vector<Statement> list;

  public StmtExprList() {
    list = new Vector<Statement>();
  }

  public void addElement(Statement n) {
    list.addElement(n);
  }

  public Statement elementAt(int i)  {
    return (Statement)list.elementAt(i);
  }

  public int size() {
    return list.size();
  }

  public void accept(Visitor v) {v.visit(this);}

  public Type accept(TypeVisitor v) {return v.visit(this);}
}
