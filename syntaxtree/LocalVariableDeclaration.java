package syntaxtree;

import visitor.TypeVisitor;
import visitor.Visitor;

import java.util.Vector;

public class LocalVariableDeclaration implements ForInit {
   public Vector<Assign2> list;
   public Type firstType;

   public LocalVariableDeclaration() {
      list = new Vector<Assign2>();
   }

   public void addElement(Assign2 n) {
      list.addElement(n);
   }

   public Assign2 elementAt(int i)  {
      return (Assign2)list.elementAt(i);
   }

   public int size() { 
      return list.size(); 
   }

   public void accept(Visitor v) {
      v.visit(this);
   }

   public Type accept(TypeVisitor v) {
      return v.visit(this);
   }
}
